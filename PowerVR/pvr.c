#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>


#include <unistd.h>

//#include "PVRScopeComms.h"
#include "PVRScopeStats.h"

// Defines
#define NO_GROUP_CHANGE 0xffffffff
// Internal control data
struct SPVRScopeImplData *PVRScopeStatsData;

// Counter information
struct SPVRScopeCounterDef *counterDefinitions;
unsigned int numCounters;
// Counter reading data
unsigned int activeGroup;
struct SPVRScopeCounterReading counterReading;

int main(int argc, char* argv[])
{
   int i;
   useconds_t sampleTime_us = 1000;
   int numTargetCounters = 0;
   int prevCounters = 0;

   activeGroup = -1;

   enum EPVRScopeInitCode eInitCode = PVRScopeInitialise(&PVRScopeStatsData);
   if(ePVRScopeInitCodeOk != eInitCode)
   {
      printf("Error initializing PVRScope 0x%x\n", eInitCode);
      exit(-1);
   }
   PVRScopeGetCounters(PVRScopeStatsData, &numCounters, &counterDefinitions, &counterReading);
   printf("Counters: %d\n", numCounters);

   if(argc == 2)
   {
      if(0 == strcmp(argv[1], "--counters"))
      {
         printf("%-30s %s\n", "Counter Name", "Group");
         for(i=0; i<numCounters; ++i)
         {
            printf("%2d %-30s %2d\n", i, counterDefinitions[i].pszName, counterDefinitions[i].nGroup);
         }
         return -2;
      }
   }

   if(argc < 4)
   {
      printf("Usage: pvr <group> <counters to wait for> <sample time in msec>\n");
      printf("Usage: pvr --counters\n");
      return -2;
   }
   else
   {
      sscanf(argv[1], "%d", &activeGroup);
      sscanf(argv[2], "%d", &numTargetCounters);
      sscanf(argv[3], "%d", &sampleTime_us);
      sampleTime_us *= 1000;
      printf("Sample time: %3.3f (ms)\n", sampleTime_us / 1000.0f);
   }
   
   PVRScopeSetGroup(PVRScopeStatsData, activeGroup);
   printf("Active group: %d\n", activeGroup);
   for(i=0; i<sampleTime_us; i+=5000)
   {
      PVRScopeReadCounters(PVRScopeStatsData, NULL);
      usleep(1 * 1000);
   }
   
   i=0;
   while(0 == PVRScopeReadCounters(PVRScopeStatsData, &counterReading))
   {
      ++i;
      usleep(100);
   }
   printf("Counter data: %d took %d rereads\n", counterReading.nValueCnt, i);

    
   for(i=0; i<counterReading.nValueCnt; ++i)
   {
      printf("[%2d] %-30s %f\n", i, counterDefinitions[i].pszName, (float)counterReading.pfValueBuf[i]);
   }

   PVRScopeDeInitialise(&PVRScopeStatsData, &counterDefinitions, &counterReading);

   return 0;
}
