using System;
using Renci.SshNet;

namespace Test_SshNet
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            AuthenticationMethod[] auth = new AuthenticationMethod[] { new PrivateKeyAuthenticationMethod("root"), new PasswordAuthenticationMethod("root", "") };
            ConnectionInfo connectInfo = new ConnectionInfo("192.168.0.103", "root", auth);
            SshClient ssh = new SshClient(connectInfo);
            Console.Write("Connecting to {0} ...", connectInfo.Host);
            ssh.Connect();
            Console.Write("Connected.");
            Console.Write("Executing remote 'cat /proc/cpuinfo' ...");
            var cmd = ssh.CreateCommand("cat /proc/cpuinfo");
            string result = cmd.Execute();
            Console.Write(result);
            ssh.Disconnect();
        }
    }
}
