﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Renci.SshNet;

namespace bigLITTLE
{
    public class CpuUltil : SshConnection
    {
        int myCpuCount = -1;
        bool[] myCpuOnline = new bool[0];

        public int CpuCount
        {
            get
            {
                return this.myCpuCount;
            }
        }

        public bool[] CpusEnabled
        {
            get
            {
                return this.myCpuOnline;
            }
        }

        public CpuUltil(Renci.SshNet.ConnectionInfo connectionInfo): base(connectionInfo)
        {
        }

        protected override void OnDisconnected()
        {
            this.myCpuCount = -1;
            base.OnDisconnected();
        }

        protected override void OnConnected()
        {
            this.myCpuCount = this.GetCpuCount();
            this.myCpuOnline = this.GetCpusOnline();
            base.OnConnected();
        }

        /// <summary>
        /// Initializing for global variables
        /// </summary>
        private int GetCpuCount()
        {
            if (!this.IsConnected)
                return -1;

            // Both of these return the number of *active* CPUs - I want physical
            //"getconf _NPROCESSORS_ONLN"
            //"grep -c ^processor /proc/cpuinfo"
            string result = this.Execute("ls -1d /sys/devices/system/cpu/cpu*");
            string[] cpus = result.Split('\n');
            //A bunch of things can have 'cpu' in the name such as cpufreq or cpuidle
            // We only want to count cpu0 .. cpuXX
            int cc = 0;
            foreach(string line in cpus)
            {
                var match = Regex.Match(line, @"cpu(\d+)$");
                if(match.Success)
                {
                    ++cc;
                }
            }
            return cc;
        }

        private bool[] GetCpusOnline()
        {
            if (!this.IsConnected)
                return new bool[0];

            int n = this.CpuCount;
            string cpusOnline = this.Execute("cat /sys/devices/system/cpu/online");
            string[] cpus = cpusOnline.Split(',');
            bool[] online = new bool[this.myCpuCount];
            foreach(string scpu in cpus)
            {
                int cpu = -1;
                if(int.TryParse(scpu, out cpu))
                {
                    if(cpu >=0 && cpu<=this.myCpuCount)
                        online[cpu] = true;
                }
                else
                {
                    if(scpu.Contains('-'))
                    {
                        //Range
                        string[] range = scpu.Split('-');
                        if (range.Length == 2)
                        {
                            int min = 0;
                            int max = 0;
                            if(int.TryParse(range[0], out min))
                            {
                                if(int.TryParse(range[1], out max))
                                {
                                    for (int i=min; i<=max; ++i)
                                        online[i] = true;
                                }
                            }
                        }
                    }
                }
            }
            return online;
        }

        Dictionary<string, ulong[]> myPreviousStats = null;
        /// <summary>
        /// Reads the (remote) CPU status information and calculates utilization per core
        /// </summary>
        /// <returns>The core utilization in a dictionary relating the remote-named mnemonic with the calculated utilzation.</returns>
        public Dictionary<string, double> GetCoreUtilization()
        {
            if (!this.IsConnected)
                return null;

            Dictionary<string, ulong[]> stats = this.FetchProcStat();
            if(myPreviousStats == null)
            {
                myPreviousStats = stats;
                return null;
            }
            Dictionary<string, double> utilization = new Dictionary<string, double>();
            foreach(var kp in stats)
            {
                if (myPreviousStats.Keys.Contains(kp.Key))
                {
                    //We have current and previous data
                    double load = this.CalcCpuUtilization(kp.Value, myPreviousStats[kp.Key]);
                    utilization.Add(kp.Key, load);
                }
            }
            myPreviousStats = stats;

            this.myCpuOnline = this.GetCpusOnline();
            return utilization;
        }

        /// <summary>
        /// Fetchs the proc stat data from the ssh/remote target
        /// </summary>
        /// <returns>The proc stat.</returns>
        private Dictionary<string, ulong[]> FetchProcStat()
        {
            if (!this.IsConnected)
                return null;

            string procstat = this.Execute("cat /proc/stat");

            Dictionary<string, ulong[]> cpuStats = new Dictionary<string, ulong[]>();

            if (string.IsNullOrEmpty(procstat))
                return cpuStats;

            string[] lines = procstat.Split('\n');
            foreach(string line in lines)
            {
                var match = Regex.Match(line, @"^(cpu[^ ]*) (.*)");
                if(match.Success)
                {
                    if (match.Captures.Count > 0)
                    {
                        if (match.Groups.Count > 1)
                        {
                            string key = match.Groups[1].ToString().Trim();
                            ulong[] stat = this.ParseCpuStat(line);
                            cpuStats.Add(key, stat);
                        }
                    }
                }
            }
            return cpuStats;
        }

        const int User = 0;
        const int Nice = 1;
        const int System = 2;
        const int Idle = 3;
        const int IOWait = 4;
        const int Irq = 5;
        const int SoftIrq = 6;
        const int Steal = 7;
        const int Guest = 8;
        const int GuestNice = 9;
        private double CalcCpuUtilization(ulong[] curStat, ulong[] prevStat)
        {
            double load = 0;

            ulong prevactive = prevStat[User] + prevStat[Nice] + prevStat[System] + prevStat[Irq] + prevStat[SoftIrq] + prevStat[Steal];
            ulong previnactive = prevStat[Idle] + prevStat[IOWait];
            ulong prevtotal = prevactive + previnactive;

            ulong active = curStat[User] + curStat[Nice] + curStat[System] + curStat[Irq] + curStat[SoftIrq] + curStat[Steal];
            ulong inactive = curStat[Idle] + curStat[IOWait];
            ulong total = active + inactive;

            ulong dtotal = total - prevtotal;
            ulong didle = inactive - previnactive;

            load = ((dtotal - didle) * 10000) / dtotal / 100.0;
            return load;
        }

        /// <summary>
        /// Parses the /proc/stat data from strings into an unsigned long array
        /// </summary>
        /// <returns>The cpu stat.</returns>
        /// <param name="cpustat">Cpustat.</param>
        private ulong[] ParseCpuStat(string cpustat)
        {
            //         user    nice   system       idle  iowait  irq   softirq  steal  guest  guest_nice
            //cpu  25053225  145238  6722636 1016493522  438544   92     59173     0       0           0
            string[] fields = cpustat.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            ulong[] stats = new ulong[fields.Length - 1];

            for(int i=0; i<stats.Length; ++i)
            {
                stats[i] = ulong.Parse(fields[i + 1]);
            }

            return stats;
        }

        public bool CpuEnable(bool enable, int cpu_id)
        {
            string sEnable = enable ? "1" : "0";
            var cmd = this.CreateCommand(string.Format("echo {0} > /sys/devices/system/cpu/cpu{1}/online", sEnable, cpu_id));
            cmd.BeginExecute();
            //string result = this.Execute(string.Format("echo {0} > /sys/devices/system/cpu/cpu{1}/online", sEnable, cpu_id));
            //this.myCpuOnline = this.GetCpusOnline();
            //return string.IsNullOrEmpty(result);
            return false;
        }

        public bool LoadCpu(out int pid, int cpu_id)
        {
            string spid  = this.Execute(string.Format("./load {0}", cpu_id));
            return int.TryParse(spid, out pid);
        }
    }
}
 