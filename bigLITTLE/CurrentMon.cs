﻿using System;
using System.Text.RegularExpressions;

namespace bigLITTLE
{
    public class CurrentMon : SshConnection
    {
        const string ofNameDvfsPwrMon = "dvfs_pwr_mon";
        const string ofNameRamPwrMon = "ram_pwr_mon";
        const string ofNameVsysPwrMon = "vsys_pwr_mon";
        string dvfsIndex = "";
        string ramIndex = "";
        string vsysIndex = "";

        public CurrentMon (Renci.SshNet.ConnectionInfo connectionInfo): base(connectionInfo)
        {
        }

        protected override void OnDisconnected()
        {
            this.dvfsIndex = "";
            this.ramIndex = "";
            this.vsysIndex = "";
            base.OnDisconnected();
        }

        protected override void OnConnected()
        {
            this.updateIndices();
            base.OnConnected();
        }

        /// <summary>
        /// Determining cpu and gpu hwmon indices
        /// </summary>
        private void updateIndices()
        {
            if (!this.IsConnected)
                return;

            string result = this.Execute("ls -ld /sys/class/hwmon/hwmon*");
            MatchCollection matches = Regex.Matches(result, @"/sys/class/hwmon/hwmon(\d+)");

            foreach (Match match in matches)
            {
                result = this.Execute("cat /sys/class/hwmon/hwmon"+match.Groups[1].ToString()+"/device/uevent");
                Match ofNameMatch = Regex.Match(result, @"OF_NAME=(\w+)");
                if (ofNameMatch.Success)
                {
                    if (ofNameDvfsPwrMon.Equals(ofNameMatch.Groups[1].ToString(), StringComparison.Ordinal))
                    {
                        dvfsIndex = match.Groups[1].ToString();
                    }
                    else if (ofNameRamPwrMon.Equals(ofNameMatch.Groups[1].ToString(), StringComparison.Ordinal))
                    {
                        ramIndex = match.Groups[1].ToString();
                    }
                    else if (ofNameVsysPwrMon.Equals(ofNameMatch.Groups[1].ToString(), StringComparison.Ordinal))
                    {
                        vsysIndex = match.Groups[1].ToString();
                    }
                    else
                    {
                    }
                }
            }
        }

        public bool GetDvfsCurrent_amps(out double current_A)
        {
            current_A = 0.0;
            if (dvfsIndex.Equals(""))
                return false;
            string result = this.Execute("cat /sys/class/hwmon/hwmon" + dvfsIndex + "/device/curr1_input");
            if (Double.TryParse(result, out current_A))
            {
                current_A /= 1000.0;
                return true;
            }
            return false;
        }

        public bool GetRamCurrent_amps(out double current_A)
        {
            current_A = 0.0;
            if (ramIndex.Equals(""))
                return false;

            string result = this.Execute("cat /sys/class/hwmon/hwmon" + ramIndex + "/device/curr1_input");
            if (Double.TryParse(result, out current_A))
            {
                current_A /= 1000.0;
                return true;
            }
            return false;
        }

        public bool GetVsysCurrent_amps(out double current_A)
        {
            current_A = 0.0;
            if (vsysIndex.Equals(""))
                return false;

            string result = this.Execute("cat /sys/class/hwmon/hwmon" + vsysIndex + "/device/curr1_input");
            if (Double.TryParse(result, out current_A))
            {
                current_A /= 1000.0;
                return true;
            }
            return false;
        }
    }
}

