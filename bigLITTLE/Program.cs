﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bigLITTLE
{
    static class Program
    {
        static internal bool AutoConnect = false;
        static internal ConnectionInfo ConnectionInfo = new ConnectionInfo();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            GraphFrm frm = new GraphFrm();
            
            try
            {
                if (args.Length > 0)
                {
                    string connect = args[0];
                    if (connect.Contains('@'))
                    {
                        Program.ConnectionInfo.Username = connect.Split('@')[0];
                        connect = connect.Substring(1 + connect.IndexOf('@'));
                    }
                    if (connect.Contains(':'))
                    {
                        Program.ConnectionInfo.Port = int.Parse(connect.Split(':')[1]);
                        connect = connect.Substring(0, connect.IndexOf(':'));
                    }
                    Program.ConnectionInfo.Hostname = connect;
                    AutoConnect = true;

                    for(int i=1; i<args.Length; ++i)
                    {
                        if(args[i] == "-A")
                            frm.ShowCpuAvgGraph = true;
                        if(args[i] == "-I")
                            frm.ShowCurrentGraph = true;
                        if(args[i] == "-G")
                            frm.ShowGpuGraph = true;
                        if(args[i] == "-B")
                        {
                            frm.BigLittleMode = true;
                            frm.Text = "R-Car big.LITTLE Demonstration";
                        }
                        if(args[i] == "-D")
                            frm.DemoMode = true;
                    }
                }
            }
            finally
            {
                //Ignore exceptions
            }

            Application.Run(frm);
        }
    }
}
