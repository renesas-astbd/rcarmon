﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bigLITTLE
{
    public partial class Graph : Form
    {
        public Graph()
        {
            InitializeComponent();
        }

        private void Graph_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Hello form load");
            this.Width = 1030;
            this.Height = 540;
            ZedGraphDemo masterPaneDemo = new MasterPaneDemo();
            this.Controls.Add(masterPaneDemo.ZedGraphControl);
            masterPaneDemo.ZedGraphControl.Width = 1000;
            masterPaneDemo.ZedGraphControl.Height = 1000;
        }
    }
}
