﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bigLITTLE
{
    public partial class ConnectionDialog : Form
    {
        private ConnectionInfo myConnectionInfo = new ConnectionInfo();

        public ConnectionDialog()
        {
            InitializeComponent();
        }


        private void ConnectionDialog_Load(object sender, EventArgs e)
        {
            this.txtIP.Text = this.myConnectionInfo.Hostname;
            this.txtUserName.Text = this.myConnectionInfo.Username;
            this.txtPassWord.Text = this.myConnectionInfo.Password;
        }

        private void bntOK_Click(object sender, EventArgs e)
        {
            myConnectionInfo.Hostname = this.txtIP.Text;
            myConnectionInfo.Username = this.txtUserName.Text;
            myConnectionInfo.Password = this.txtPassWord.Text;
            this.DialogResult = DialogResult.OK;
        }

        private void bntCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        public ConnectionInfo ConnectionInfo
        {
            get
            {
                return this.myConnectionInfo;
            }
            set
            {
                this.myConnectionInfo = value;
            }
        }
    }
}
