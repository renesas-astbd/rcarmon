﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ZedGraph;

namespace bigLITTLE
{
    public partial class GraphFrm : Form
    {
        CpuUltil myCpuUltil = null;
        GpuUtilization myGpuUtil = null;
        CurrentMon myCurrentMon = null;
        List<ZedGraphControl> myCpuGraphs = new List<ZedGraphControl>();
        ZedGraphControl myAvgGraph = new ZedGraphControl();
        ZedGraphControl myGpuGraph = new ZedGraphControl();
        ZedGraphControl myCurrentGraph = new ZedGraphControl();
        const int xAxisRange = 60; // Show data for 60 seconds

        bool bShowCpuAvgGraph = false;
        bool bShowCurrentGraph = false;
        bool bShowGpuGraph = false;
        bool bBigLittleMode = false;
        bool bDemoMode = false;

        public GraphFrm()
        {
            InitializeComponent();
        }

        public bool ShowCpuAvgGraph
        {
            set
            {
                this.bShowCpuAvgGraph = value;
            }
        }

        public bool ShowCurrentGraph
        {
            set
            {
                this.bShowCurrentGraph = value;
            }
        }

        public bool ShowGpuGraph
        {
            set
            {
                this.bShowGpuGraph = value;
            }
        }

        public bool BigLittleMode
        {
            set
            {
                this.bBigLittleMode = value;
            }
        }

        public bool DemoMode
        {
            set
            {
                this.bDemoMode = value;
            }
        }

        private void GraphFrm_Load(object sender, EventArgs e)
        {
            this.flowLayoutPanel1.AutoScroll = true;
            this.toolStripStatusLabelConnection.Text = "Not connected";
            Application.DoEvents();
        }

        private void GraphFrm_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();
            if (Program.AutoConnect)
            {
                this.connectToolStripMenuItem_Click(this, new EventArgs());
            }
            if (this.bDemoMode)
                this.timer3.Start();
        }

        private void GraphFrm_Resize(object sender, EventArgs e)
        {
            var size = CalcIdealGraphSize();
            foreach(ZedGraphControl graph in this.flowLayoutPanel1.Controls)
            {
                graph.Height = size.Item1;
                graph.Width = size.Item2;
            }
            foreach(ZedGraphControl graph in this.flowLayoutPanel1.Controls)
            {
                graph.Refresh();
            }
        }

        Tuple<int, int> CalcIdealGraphSize(int graphs = -1)
        {
            if(graphs == -1)
                graphs = this.flowLayoutPanel1.Controls.Count;
            graphs = graphs < 1 ? 1 : graphs;
            int h = this.flowLayoutPanel1.Height;
            int w = this.flowLayoutPanel1.Width / graphs;
            const int minwidth = 384;
            if(w < minwidth)
            {
                int fits = this.flowLayoutPanel1.Width / minwidth;
                w = this.flowLayoutPanel1.Width / fits;
                int rows = graphs / fits;
                rows += graphs % fits > 0 ? 1 : 0;
                h = this.flowLayoutPanel1.Height / rows;
            }
            return new Tuple<int,int>(h-5, w-6);
        }

        void configureGraphCpu(string name, ZedGraphControl graphControl, Tuple<int,int> size = null)
        {
            if (size == null)
            {
                graphControl.Height = 384;
                graphControl.Width = 384;
            }
            else
            {
                graphControl.Height = size.Item1;
                graphControl.Width = size.Item2;
            }

            graphControl.Tag = 0.0;

            GraphPane graphPane = graphControl.GraphPane;
            graphPane.Title.Text = name;
            graphPane.Title.IsVisible = true;
            graphPane.Legend.IsVisible = false;
            graphPane.IsPenWidthScaled = false;

            graphPane.XAxis.Title.Text = "Time (sec)";
            graphPane.YAxis.Title.Text = "CPU %";

            graphPane.YAxis.Scale.Min = 0;
            graphPane.YAxis.Scale.Max = 100;

            graphPane.XAxis.Scale.Min = 0;
            graphPane.XAxis.Scale.Max = xAxisRange;

            graphPane.XAxis.Scale.MajorStep = 10.0f;
            graphPane.XAxis.Scale.MinorStep = 5.0f;
            graphPane.YAxis.Scale.MajorStep = 10.0f;
            graphPane.YAxis.Scale.MinorStep = 5.0f;

            graphPane.Fill = new Fill(Color.White, Color.LightYellow, 45.0F);
            // Reduce the base dimension to 6 inches, since these panes tend to be smaller
            graphPane.BaseDimension = 6.0f;
            PointPairList list = new PointPairList(); // To to update the XAxis so it follows the points which are using time as their X values. (1)
            LineItem curve = graphPane.AddCurve(name, list, Color.Green, SymbolType.None);
            curve.Line.Fill = new Fill(Color.White, Color.Green, 45.0f);
        }

        void configureGraphGpu(string name, ZedGraphControl graphControl, Tuple<int,int> size = null)
        {
            this.configureGraphCpu(name, graphControl, size);
            GraphPane graphPane = graphControl.GraphPane;
            graphPane.YAxis.Title.Text = "GPU %";
            graphPane.CurveList.Clear();
            PointPairList list = new PointPairList(); // To to update the XAxis so it follows the points which are using time as their X values. (1)
            LineItem curve = graphPane.AddCurve(name, list, Color.Purple, SymbolType.None);
            list = new PointPairList(); // To to update the XAxis so it follows the points which are using time as their X values. (1)
            curve = graphPane.AddCurve(name, list, Color.Red, SymbolType.None);
            //curve.Line.Fill = new Fill(Color.White, Color.Purple, 45.0f);
        }

        void configureGraphCurrent(string name0, string name1, string name2, ZedGraphControl graphControl, Tuple<int,int> size = null)
        {
            this.configureGraphCpu(name0, graphControl, size);
            GraphPane graphPane = graphControl.GraphPane;
            graphPane.Title.Text = "Current";
            graphPane.YAxis.Title.Text = "Current (A)";
            graphPane.YAxis.Scale.Max = 3;
            graphPane.YAxis.Scale.MajorStep = 0.5f;
            graphPane.YAxis.Scale.MinorStep = 0.25f;
            graphPane.CurveList.Clear();
            PointPairList list = new PointPairList();
            LineItem curve = graphPane.AddCurve(name0, list, Color.Red, SymbolType.None);
            list = new PointPairList();
            curve = graphPane.AddCurve(name1, list, Color.Green, SymbolType.None);
            list = new PointPairList();
            curve = graphPane.AddCurve(name2, list, Color.Blue, SymbolType.None);
        }

        private void CreateGraphs()
        {
            var idealSize = CalcIdealGraphSize(this.myCpuUltil.CpuCount + (bShowCurrentGraph ? 1 : 0) + (bShowGpuGraph ? 1 : 0));

            if (bShowCpuAvgGraph)
            {
                this.configureGraphCpu("CPU Avg.", myAvgGraph, idealSize);
                this.flowLayoutPanel1.Controls.Add(myAvgGraph);
            }

            if (bShowCurrentGraph)
            {
                this.configureGraphCurrent("CPU Current (A)", "RAM Current (A)", "Vsys Current (A)", myCurrentGraph, idealSize);
                this.flowLayoutPanel1.Controls.Add(myCurrentGraph);
            }

            if (bShowGpuGraph)
            {
                this.configureGraphGpu("GPU", myGpuGraph, idealSize);
                this.flowLayoutPanel1.Controls.Add(myGpuGraph);
            }

            for (int i = 0; i < this.myCpuUltil.CpuCount; ++i)
            {
                ZedGraphControl graph = new ZedGraphControl();
                double running = 0.0;
                graph.Tag = running;
                myCpuGraphs.Add(graph);
                if(this.bBigLittleMode)
                {
                    if(i < 4)
                        this.configureGraphCpu("big " + i, graph, idealSize);
                    else
                        this.configureGraphCpu("LITTLE " + i, graph, idealSize);
                }
                else
                    this.configureGraphCpu("CPU " + i, graph, idealSize);
                this.flowLayoutPanel1.Controls.Add(graph);
                graph.Click += graph_OnClick;
            }
        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void configureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConnectionDialog dlg = new ConnectionDialog();
            dlg.ConnectionInfo = Program.ConnectionInfo;
            dlg.ShowDialog();
            switch (dlg.DialogResult)
            {
                case System.Windows.Forms.DialogResult.OK:
                    Program.ConnectionInfo = dlg.ConnectionInfo;
                    break;
            }

        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.toolStripStatusLabelConnection.Text = "Connecting ...";
                Application.DoEvents();
                this.myCpuUltil = new CpuUltil(SshConnection.CreateSshConnectInfo(Program.ConnectionInfo));
                this.myCpuUltil.ErrorOccurred += CpuUltil_ErrorOccurred;
                this.myCpuUltil.OnConnect += CpuUltil_OnConnect;
                this.myCpuUltil.OnDisconnect += CpuUltil_OnDisconnect;
                this.myCpuUltil.Connect();

                this.myGpuUtil = new GpuUtilization(SshConnection.CreateSshConnectInfo(Program.ConnectionInfo));
                this.myGpuUtil.OnGpuData += GpuData_OnGpuData;
                if(this.bShowGpuGraph)
                    this.myGpuUtil.Connect();

                this.myCurrentMon = new CurrentMon(SshConnection.CreateSshConnectInfo(Program.ConnectionInfo));
                if(this.bShowCurrentGraph)
                    this.myCurrentMon.Connect();
            }
            catch(Exception ex)
            {
                this.toolStripStatusLabelConnection.Text = ex.Message;
            }
        }

        void GpuData_OnGpuData (object sender, Dictionary<string, double> gpuUtil)
        {
            double fps = gpuUtil["Frames per second (FPS)"];
            double tiler = gpuUtil["Tiler active"];

            double t = this.myTimer.ElapsedMilliseconds / 1000.0;
            myGpuGraph.GraphPane.XAxis.Scale.Min = Math.Max(t - xAxisRange, 0);
            myGpuGraph.GraphPane.XAxis.Scale.Max = Math.Max(t, xAxisRange);
            myGpuGraph.AxisChange();

            if (fps < 10000 && fps > 0)
            {
                this.myGpuGraph.GraphPane.CurveList[0].AddPoint(t, fps);
                if (this.myGpuGraph.GraphPane.CurveList[0].Points[0].X <= myGpuGraph.GraphPane.XAxis.Scale.Min)
                    this.myGpuGraph.GraphPane.CurveList[0].RemovePoint(0);
            }

            if (tiler < 100)
            {
                this.myGpuGraph.GraphPane.CurveList[1].AddPoint(t, tiler);
                if (this.myGpuGraph.GraphPane.CurveList[1].Points[0].X <= myGpuGraph.GraphPane.XAxis.Scale.Min)
                    this.myGpuGraph.GraphPane.CurveList[1].RemovePoint(0);
            }
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.toolStripStatusLabelConnection.Text = "";
            Application.DoEvents();
            this.timer1.Enabled = false;
            if (this.myCpuUltil != null)
            {
                this.myCpuUltil.Disconnect();
                this.myCpuUltil.Dispose();
                this.myCpuUltil = null;
            }
        }

        void RemoveGraphs()
        {
            this.flowLayoutPanel1.Controls.Clear();
            this.myCpuGraphs = null;
            this.myAvgGraph = null;
            this.myCurrentGraph = null;
            this.myGpuGraph = null;
            System.GC.Collect();
        }

        private void CpuUltil_ErrorOccurred(object sender, Renci.SshNet.Common.ExceptionEventArgs e)
        {
            this.RemoveGraphs();
            toolStripStatusLabelConnection.Text = e.Exception.Message;
            Application.DoEvents();
            if (this.myCpuUltil != null)
            {
                this.myCpuUltil.Disconnect();
                this.myCpuUltil.Dispose();
                this.myCpuUltil = null;
            }
        }

        System.Diagnostics.Stopwatch myTimer = new System.Diagnostics.Stopwatch();
        private void timer1_Tick(object sender, EventArgs e)
        {
            double dt = timer1.Interval / 1000.0f;
            double Hz = 1.0 / dt;

            if (this.myCpuUltil != null)
            {
                if (this.myCpuUltil.IsConnected)
                {
                    Dictionary<string, double> utilization = this.myCpuUltil.GetCoreUtilization();
                    double t = this.myTimer.ElapsedMilliseconds / 1000.0;
                    if (null == utilization)
                        return;

                    if (bShowCpuAvgGraph)
                    {
                        myAvgGraph.GraphPane.XAxis.Scale.Min = Math.Max(t - xAxisRange, 0);
                        myAvgGraph.GraphPane.XAxis.Scale.Max = Math.Max(t, xAxisRange);
                        if (this.myAvgGraph.GraphPane.CurveList[0].NPts > xAxisRange * Hz)
                        {
                            this.myAvgGraph.GraphPane.CurveList[0].RemovePoint(0);
                        }
                        if (utilization.Keys.Contains("cpu"))
                        {
                            this.myAvgGraph.GraphPane.CurveList[0].AddPoint(t, utilization["cpu"]);
                            double average = this.CalcRunningAverage(this.myAvgGraph);
                            this.myAvgGraph.Tag = average;
                            this.myAvgGraph.GraphPane.Title.Text = string.Format("Tot. CPU - {0:F2} %\naverage - {1:F2}%", utilization["cpu"], average);
                        }
                        else
                        {
                            this.myAvgGraph.GraphPane.CurveList[0].AddPoint(t, 0);
                            this.myAvgGraph.GraphPane.Title.Text = string.Format("CPU Avg. - Off");
                        }
                        myAvgGraph.AxisChange();
                    }

                    for (int i = 0; i < this.myCpuGraphs.Count; ++i)
                    {
                        // Remove not shown point to prvent overflow
                        if (this.myCpuGraphs[i].GraphPane.CurveList[0].NPts > xAxisRange * Hz)
                        {
                            this.myCpuGraphs[i].GraphPane.CurveList[0].RemovePoint(0);
                        }
                        this.myCpuGraphs[i].GraphPane.XAxis.Scale.Min = Math.Max(t - xAxisRange, 0);
                        this.myCpuGraphs[i].GraphPane.XAxis.Scale.Max = Math.Max(t, xAxisRange);
                        this.myCpuGraphs[i].AxisChange();
                        string cpuName = this.bBigLittleMode ? (i<4 ? "big" : "LITTLE") : "CPU";
                        if (utilization.Keys.Contains("cpu" + i))
                        {
                            this.myCpuGraphs[i].GraphPane.CurveList[0].AddPoint(t, utilization["cpu" + i]); // Because we also have average cpu load at index 0

                            double average = this.CalcRunningAverage(this.myCpuGraphs[i]);
                            this.myCpuGraphs[i].Tag = average;
                            this.myCpuGraphs[i].GraphPane.Title.Text = string.Format("{0} {1} - {2:F2}%\naverage - {3:F2}%", cpuName, i, utilization["cpu" + i], average);
                        }
                        else
                        {
                            this.myCpuGraphs[i].GraphPane.CurveList[0].AddPoint(t, 0);
                            this.myCpuGraphs[i].GraphPane.Title.Text = string.Format("{0} {1} - Off", cpuName, i);
                        }
                    }
                    t += (this.timer1.Interval * 1.0) / 1000.0; // Update XAxis value base on sample rate (seconds)
                    TimeSpan ts = new TimeSpan(0, 0, (int)t);
                    this.toolStripStatusLabelConnection.Text = string.Format("Time {0:00}:{1:00}", ts.TotalMinutes, ts.Seconds);

                    for (int i = 0; i < this.myCpuGraphs.Count; ++i)
                    {
                        this.myCpuGraphs[i].Refresh();
                    }
                    this.myGpuGraph.Refresh();
                    this.myCurrentGraph.Refresh();
                    this.myAvgGraph.Refresh();
                }
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (bShowCurrentGraph)
            {
                double t = this.myTimer.ElapsedMilliseconds / 1000.0;
                double dt = timer2.Interval / 1000.0f;
                double Hz = 1.0 / dt;

                myCurrentGraph.GraphPane.XAxis.Scale.Min = Math.Max(t - xAxisRange, 0);
                myCurrentGraph.GraphPane.XAxis.Scale.Max = Math.Max(t, xAxisRange);
                if (this.myCurrentGraph.GraphPane.CurveList[0].NPts > xAxisRange * Hz)
                    this.myCurrentGraph.GraphPane.CurveList[0].RemovePoint(0);
                if (this.myCurrentGraph.GraphPane.CurveList[1].NPts > xAxisRange * Hz)
                    this.myCurrentGraph.GraphPane.CurveList[1].RemovePoint(0);
                if (this.myCurrentGraph.GraphPane.CurveList[2].NPts > xAxisRange * Hz)
                    this.myCurrentGraph.GraphPane.CurveList[2].RemovePoint(0);
                if (this.myCurrentMon != null)
                {
                    double dvfsI_A;
                    double ramI_A;
                    double vsysI_A;

                    if(this.myCurrentMon.GetDvfsCurrent_amps(out dvfsI_A))
                        this.myCurrentGraph.GraphPane.CurveList[0].AddPoint(t, dvfsI_A);
                    if(this.myCurrentMon.GetRamCurrent_amps(out ramI_A))
                        this.myCurrentGraph.GraphPane.CurveList[1].AddPoint(t, ramI_A);
                    if(this.myCurrentMon.GetVsysCurrent_amps(out vsysI_A))
                        this.myCurrentGraph.GraphPane.CurveList[2].AddPoint(t, vsysI_A);
                }
                else
                {
                    this.myCurrentGraph.GraphPane.CurveList[0].AddPoint(t, 0);
                    this.myCurrentGraph.GraphPane.CurveList[1].AddPoint(t, 0);
                    this.myCurrentGraph.GraphPane.CurveList[2].AddPoint(t, 0);
                }
                myCurrentGraph.AxisChange();
            }
        }

        double CalcRunningAverage(ZedGraphControl graph)
        {
            int n = graph.GraphPane.CurveList[0].Points.Count;
            double sum = 0.0;
            for (int i = 0; i < n; ++i)
            {
                sum += graph.GraphPane.CurveList[0].Points[i].Y;
            }
            return (sum / n);
        }

        void CpuUltil_OnConnect(object sender, EventArgs e)
        {
            if (this.myCpuUltil.IsConnected)
            {
                this.CreateGraphs();
                this.toolStripStatusLabelConnection.Text = "Connected";
                this.myTimer.Start();
                this.timer1.Enabled = true;
                if (bShowCurrentGraph)
                    this.timer2.Enabled = true;
            }
        }

        void CpuUltil_OnDisconnect(object sender, EventArgs e)
        {
            this.RemoveGraphs();
        }

        void graph_OnClick(object sender, EventArgs e)
        {
            ZedGraphControl graph = sender as ZedGraphControl;
            if(graph != null)
            {
                int idx = this.myCpuGraphs.FindIndex((ZedGraphControl obj) => obj == graph);
                if(idx >=0)
                {
                    bool[] cpusOnline = this.myCpuUltil.CpusEnabled;
                    if (idx <= cpusOnline.Length)
                    {
                        bool online = cpusOnline[idx];
                        string cpuName = this.bBigLittleMode ? (idx<4 ? "big" : "LITTLE") : "CPU";
                        if (!online)
                        {
                            //CPU is off, turn it on
                            if (this.myCpuUltil.CpuEnable(true, idx))
                            {
                                this.myCpuGraphs[idx].GraphPane.Title.Text = string.Format("{0} {1} - --%\naverage - --%", cpuName, idx);
                                this.myCpuGraphs[idx].Refresh();
                            }
                        }
                        else
                        {
                            //CPU is on, turn it off
                            if (this.myCpuUltil.CpuEnable(false, idx))
                            {
                                this.myCpuGraphs[idx].GraphPane.Title.Text = string.Format("{0} {1} - Off", cpuName, idx);
                                this.myCpuGraphs[idx].Refresh();
                            }
                        }
                    }
                }
            }
        }

        enum DemoPhase
        {
            Start,
            LowUtilization,
            BigUtilization,
            LittleUtilization,
            FullUtilization
        }
        DemoPhase demoPhase = DemoPhase.Start;
        int demoCpu = 0;
        int[] pids;
        private void timer3_Tick(object sender, EventArgs e)
        {
            int pid;
            int demo2;

            //Run demo state-machine
            switch(demoPhase)
            {
                case DemoPhase.Start:
                    pids = new int[this.myCpuUltil.CpuCount];
                    for (int i=0; i<this.myCpuUltil.CpuCount; ++i)
                    {
                        this.myCpuUltil.CpuEnable(true, i);
                        pids[i] = 0;
                    }
                    demoCpu = 0;
                    demoPhase = DemoPhase.FullUtilization;
                    break;

                case DemoPhase.FullUtilization:
                    if(pids[demoCpu] != 0)
                    {
                        this.myCpuUltil.Execute(string.Format("kill {0}", pids[demoCpu]));
                        pids[demoCpu] = 0;
                    }
                    this.myCpuUltil.CpuEnable(true, demoCpu);
                    if(this.myCpuUltil.LoadCpu(out pid, demoCpu))
                        pids[demoCpu] = pid;

                    ++demoCpu;
                    if (demoCpu >= this.myCpuUltil.CpuCount)
                    {
                        demoCpu = this.myCpuUltil.CpuCount/2; //Start with first LITTLE
                        demoPhase = DemoPhase.BigUtilization;
                    }
                    break;
                
                case DemoPhase.BigUtilization:
                    //demoCpu = 4
                    if(pids[demoCpu] != 0)
                    {
                        this.myCpuUltil.Execute(string.Format("kill {0}", pids[demoCpu]));
                        pids[demoCpu] = 0;
                    }
                    this.myCpuUltil.CpuEnable(false, demoCpu);

                    ++demoCpu;
                    if (demoCpu >= this.myCpuUltil.CpuCount / 2)
                    {
                        demoCpu = 0;
                        demoPhase = DemoPhase.LittleUtilization;
                    }
                    break;
                
                case DemoPhase.LittleUtilization:
                    //demoCpu = 0
                    int little = demoCpu + this.myCpuUltil.CpuCount / 2;
                    this.myCpuUltil.CpuEnable(true, little);
                    if(pids[demoCpu] != 0)
                    {
                        this.myCpuUltil.Execute(string.Format("kill {0}", pids[demoCpu]));
                        pids[demoCpu] = 0;
                    }
                    this.myCpuUltil.CpuEnable(false, demoCpu);

                    if (this.myCpuUltil.LoadCpu(out pid, little))
                        pids[demoCpu] = pid;

                    ++demoCpu;
                    if (demoCpu >= this.myCpuUltil.CpuCount / 2)
                    {
                        demoCpu = 0;
                        demoPhase = DemoPhase.LowUtilization;
                    }
                    break;
                
                case DemoPhase.LowUtilization:
                    if(pids[demoCpu] != 0)
                    {
                        this.myCpuUltil.Execute(string.Format("kill {0}", pids[demoCpu]));
                        pids[demoCpu] = 0;
                    }
                    this.myCpuUltil.CpuEnable(true, demoCpu);
                    ++demoCpu;
                    if (demoCpu >= this.myCpuUltil.CpuCount)
                    {
                        demoCpu = 0;
                        demoPhase = DemoPhase.FullUtilization;
                    }
                    break;
            }
        }
    }
}
