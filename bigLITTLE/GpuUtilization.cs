using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace bigLITTLE
{
    public class GpuUtilization : SshConnection
    {
        public event EventHandler<Dictionary<string, double>> OnGpuData;

        public GpuUtilization(Renci.SshNet.ConnectionInfo connectionInfo) : base(connectionInfo)
        {
            base.OnConnect += SshClient_OnConnect;
        }

        public void SshClient_OnConnect(object sender, EventArgs e)
        {
            this.StartGetGpuUtilization();
        }

        bool bInProgress = false;
        public bool InProgress
        {
            get
            {
                return this.bInProgress;
            }
        }

        public void StartGetGpuUtilization()
        {
            Renci.SshNet.SshCommand sshcmd = this.CreateCommand("./pvr 9 1 300");
            sshcmd.BeginExecute(GetGpuUtilizationFinished, sshcmd);
            bInProgress = true;
        }

        void GetGpuUtilizationFinished(IAsyncResult result)
        {
            Renci.SshNet.SshCommand sshcmd = result.AsyncState as Renci.SshNet.SshCommand;
            bInProgress = false;
            this.StartGetGpuUtilization();

            Dictionary<string, double> gpuUtil = new Dictionary<string, double>();
            string[] pvr = sshcmd.Result.Split(new char[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);

            if (pvr.Length < 20)
                return;

            foreach(string line in pvr)
            {
                if (!line.StartsWith("["))
                    continue;
                //int id = int.Parse(line.Substring(1, 2));
                string name = line.Substring(5, 31).Trim();
                double value = double.Parse(line.Substring(36));
                gpuUtil.Add(name, value);
            }

            if(OnGpuData != null)
            {
                OnGpuData(this, gpuUtil);
            }

            return;
        }
    }
}

