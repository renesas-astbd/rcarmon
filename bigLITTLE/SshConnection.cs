using System;
using System.IO;
using System.Collections.Generic;
using Renci.SshNet;

namespace bigLITTLE
{
    public class SshConnection : SshClient
    {
        //Hide base-class handler
        public new event EventHandler<Renci.SshNet.Common.ExceptionEventArgs> ErrorOccurred;
        public event EventHandler OnConnect;
        public event EventHandler OnDisconnect;

        public static Renci.SshNet.ConnectionInfo CreateSshConnectInfo(bigLITTLE.ConnectionInfo connectionInfo)
        {
            List<PrivateKeyFile> privateKeys = SshConnection.LoadKeys();
            List<AuthenticationMethod> methods = new List<AuthenticationMethod>();

            methods.Add(new PasswordAuthenticationMethod(connectionInfo.Username, connectionInfo.Password));
            methods.Add(new PrivateKeyAuthenticationMethod(connectionInfo.Username, privateKeys.ToArray()));

            Renci.SshNet.ConnectionInfo sshInfo = new Renci.SshNet.ConnectionInfo(connectionInfo.Hostname, connectionInfo.Port, connectionInfo.Username, methods.ToArray());
            return sshInfo;
        }

        static protected List<PrivateKeyFile> LoadKeys()
        {
            List<PrivateKeyFile> privateKeys = new List<PrivateKeyFile>();

            //Read private keys
            string keysPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Keys");
            if (!Directory.Exists(keysPath))
            {
                keysPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), ".ssh");
                if (!Directory.Exists(keysPath))
                {
                    //Empty list
                    return privateKeys;
                }
            }

            string[] privateKeysPaths = Directory.GetFiles(keysPath, "*.rsa", SearchOption.AllDirectories);
            foreach (string key in privateKeysPaths)
            {
                privateKeys.Add(new PrivateKeyFile(key));
            }
            privateKeysPaths = Directory.GetFiles(keysPath, "id_rsa", SearchOption.AllDirectories);
            foreach (string key in privateKeysPaths)
            {
                privateKeys.Add(new PrivateKeyFile(key));
            }
            return privateKeys;

        }

        public SshConnection(Renci.SshNet.ConnectionInfo connectionInfo)
            : base(connectionInfo)
        {
            base.ErrorOccurred += this.OnErrorOccurred;
        }

        // Forward errors from base-class to our class' error handler
        private void OnErrorOccurred(object sender, Renci.SshNet.Common.ExceptionEventArgs e)
        {
            if (this.ErrorOccurred != null)
            {

                this.ErrorOccurred(this, e);
            }
        }

        protected override void OnDisconnecting()
        {
            base.OnDisconnecting();
        }

        protected override void OnDisconnected()
        {
            base.OnDisconnected();
            if(OnDisconnect != null)
            {
                EventArgs e = new EventArgs();
                OnDisconnect(this, e);
            }
        }

        protected override void OnConnecting()
        {
            base.OnConnecting();
        }

        protected override void OnConnected()
        {
            base.OnConnected();
            if(OnConnect != null)
            {
                EventArgs e = new EventArgs();
                OnConnect(this, e);
            }
        }

        public string Execute(string cmd)
        {
            try
            {
                var sshcmd = this.CreateCommand(cmd);
                return sshcmd.Execute(cmd);
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}

