﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bigLITTLE
{
    public class ConnectionInfo
    {
        private string myHostname = "192.168.0.100";
        private String myUsername = "root";
        private String myPassword = string.Empty;
        private int myPort = 22;

        public ConnectionInfo()
        { 
            
        }

        public ConnectionInfo(String ip, String user)
        {
            myHostname = ip;
            user = myUsername;
        }

        public ConnectionInfo(System.Net.IPAddress ip, String user, int port)
        {
            myHostname = ip.ToString();
            user = myUsername;
            this.myPort = port;
        }

        public ConnectionInfo(String ip, String user, String pass)
        {
            myHostname = ip;
            myUsername = user;
            myPassword = pass;
        }

        public String Hostname
        {
            get
            {
                return myHostname.ToString();
            }
            set
            {
                myHostname = value;
            }
        }

        public String Username
        {
            get
            {
                return myUsername;
            }
            set
            {
                myUsername = value;
            }
        }

        public String Password
        {
            get
            {
                return myPassword;
            }
            set
            {
                myPassword = value;
            }
        }

        public int Port
        {
            get
            {
                return this.myPort;
            }

            set
            {
                this.myPort = value;
            }
        }
    }
}
